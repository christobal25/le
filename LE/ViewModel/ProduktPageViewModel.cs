﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;

namespace LE.ViewModel
{
    public class ProduktPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        public RelayCommand HomeNavigation { get; private set; }

        public ProduktPageViewModel()
        {
            _navigationService = ViewModelLocator.NavService;
            HomeNavigation = new RelayCommand(NavigateHome);
        }

        private void NavigateHome()
        {
            _navigationService.NavigateTo(Constants.MainPageKey);
        }
    }
}