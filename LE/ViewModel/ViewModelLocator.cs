﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using Microsoft.Practices.ServiceLocation;
using LE.Views;
using static LE.Constants;

namespace LE.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
            NavService = new NavigationService();
            NavService.Configure(MainPageKey, typeof(MainPage));
            NavService.Configure(ProduktPageKey, typeof(ProduktPage));
            NavService.Configure(KatalogPageKey, typeof(KatalogPage));
            if (ViewModelBase.IsInDesignModeStatic)
            {
                // Create design time view services and models
            }
            else
            {
                // Create run time view services and models
            }

            //Register your services used here
            SimpleIoc.Default.Register<INavigationService>(() => NavService);
            SimpleIoc.Default.Register<MainPageViewModel>();
            SimpleIoc.Default.Register<ProduktPageViewModel>();
            SimpleIoc.Default.Register<KatalogViewModel>();
        }

        public MainPageViewModel MainPage => ServiceLocator.Current.GetInstance<MainPageViewModel>();
        public ProduktPageViewModel ProduktPage => ServiceLocator.Current.GetInstance<ProduktPageViewModel>();
        public KatalogViewModel KatalogPage => ServiceLocator.Current.GetInstance<KatalogViewModel>();


        internal static NavigationService NavService { get; set; }
        // <summary>
        // The cleanup.
        // </summary>
        public static void Cleanup()
        {
            //  Clear the ViewModels
        }
    }
}