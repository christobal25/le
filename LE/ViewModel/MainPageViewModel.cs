﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;

namespace LE.ViewModel
{
    public class MainPageViewModel: ViewModelBase
    {
        private readonly INavigationService _navigationService;
        public RelayCommand ProduktNavigateCommand { get; private set; }
        public RelayCommand KatalogNavigateCommand { get; set; }
        //public MainPageViewModel(INavigationService navigationService)
        //{
        //    _navigationService = navigationService;        
        //    NavigateCommand = new RelayCommand(NavigateCommandAction);
        //}

        public MainPageViewModel()
        {
            _navigationService = ViewModelLocator.NavService;
            ProduktNavigateCommand = new RelayCommand(NavigateProdukt);
            KatalogNavigateCommand = new RelayCommand(NavigateKatalog);
        }


        private void NavigateProdukt()
        {
            // Do Something
            _navigationService.NavigateTo(Constants.ProduktPageKey);
        }

        private void NavigateKatalog()
        {
            _navigationService.NavigateTo(Constants.KatalogPageKey);
        }
    }
}
