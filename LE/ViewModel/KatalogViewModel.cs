﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;

namespace LE.ViewModel
{
    public class KatalogViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        public RelayCommand HomeNavigation { get; private set; }

        public KatalogViewModel()
        {
            _navigationService = ViewModelLocator.NavService;
            HomeNavigation = new RelayCommand(NavigateHome);
        }

        private void NavigateHome()
        {
            _navigationService.NavigateTo(Constants.MainPageKey);
        }
    }
}